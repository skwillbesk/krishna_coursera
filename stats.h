/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file stats.h
 * @brief header file for stats.c
 *
 * Contains macro, function declarations for stats.c
 *
 * @author Shamantha Krishna K G
 * @date 15 June 2020
 *
 */
#ifndef __STATS_H__
#define __STATS_H__


/**
 * @brief Displays the statistics of the array to the screen
 *
 * This function takes an unsigned char pointer to an n-element data array
 * and an unsigned integer as the size of the array and prints out the statistics
 * of the array including minimum, maximum, mean and median on the screen
 *
 * @param list A unsigned char pointer to an n-element data array
 * @param size An unsigned integer as the size of the array
 */
void print_statistics(unsigned char* list,unsigned int size);


/**
 * @brief Displays the contents of the array to the screen
 *
 * This function takes an unsigned char pointer to an n-element data array
 * and an unsigned integer as the size of the array and prints out the contents
 * on the screen
 *
 * @param list A unsigned char pointer to an n-element data array
 * @param size An unsigned integer as the size of the array
 */
void print_array(unsigned char* list,unsigned int size);


/**
 * @brief returns the median of the array
 *
 * This function takes an unsigned char pointer to an n-element data array
 * and an unsigned integer as the size of the array and returns the
 * median of the array
 *
 * @param list        An unsigned char pointer to an n-element data array
 * @param size        An unsigned integer , the size of the array
 * @return median     An unsigned char ,median of the array
 */
unsigned char find_median(unsigned char* list,unsigned int size);


/**
 * @brief returns the mean of the array
 *
 * This function takes an unsigned char pointer to an n-element data array
 * and an unsigned integer as the size of the array and returns the
 * mean of the array
 *
 * @param list        An unsigned char pointer to an n-element data array
 * @param size        An unsigned integer , the size of the array
 * @return mean       An unsigned char ,mean of the array
 */
unsigned char find_mean(unsigned char* list,unsigned int size);


/**
 * @brief returns the maximum element in the array
 *
 * This function takes an unsigned char pointer to an n-element data array
 * and an unsigned integer as the size of the array and returns the
 * maximum element in the array
 *
 * @param list        An unsigned char pointer to an n-element data array
 * @param size        An unsigned integer , the size of the array
 * @return maximum    An unsigned char ,maximum element in the array
 */
unsigned char find_maximum(unsigned char* list,unsigned int size);


/**
 * @brief returns the minimum element in the array
 *
 * This function takes an unsigned char pointer to an n-element data array
 * and an unsigned integer as the size of the array and returns the
 * minimum element in the array
 *
 * @param list        An unsigned char pointer to an n-element data array
 * @param size        An unsigned integer , the size of the array
 * @return minimum    An unsigned char ,minimum element in the array
 */
unsigned char find_minimum(unsigned char* list,unsigned int size);


/**
 * @brief sorts the element in the array
 *
 * This function takes an unsigned char pointer to an n-element data array
 * and an unsigned integer as the size of the array and
 * sorts the array from largest to smallest.
 * (The zeroth Element  the largest value, and the last element (n-1) , the smallest value.)
 *
 * @param list        An unsigned char pointer to an n-element data array
 * @param size        An unsigned integer , the size of the array
 */
void sort_array(unsigned char* list,unsigned int size);


#endif /* __STATS_H__ */
