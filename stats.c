/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file stats.c
 * @brief A simple C program that can perform analysis on array of  data items
 *
 * A simple C program that can analyze an array of unsigned char data items and
 * report analytics on the maximum, minimum, mean, and median of the data set
 *
 * @author Shamantha Krishna K G
 * @date 15 June 2020
 *
 */



#include <stdio.h>
#include "stats.h"

/* Size of the Data Set */
#define SIZE (40)

void main() {

  unsigned char test[SIZE] = { 34, 201, 190, 154,   8, 194,   2,   6,
                              114, 88,   45,  76, 123,  87,  25,  23,
                              200, 122, 150, 90,   92,  87, 177, 244,
                              201,   6,  12,  60,   8,   2,   5,  67,
                                7,  87, 250, 230,  99,   3, 100,  90};

  /* Other Variable Declarations Go Here */
  /* Statistics and Printing Functions Go Here */
  unsigned int size = SIZE;
  print_array(test,size);
  print_statistics(test,size);
  printf("The array sorted from higher to lower is \n\n");
  print_array(test,size);
}

void print_statistics(unsigned char* list,unsigned int size){
  printf("The maximum elment in the array is %d\n\n",find_maximum(list,size));
  printf("The minimum elment in the array is %d\n\n",find_minimum(list,size));
  printf("The mean of the array is %d\n\n",find_mean(list,size));
  printf("The median of the array is %d\n\n",find_median(list,size));
  
}

void print_array(unsigned char* list,unsigned int size){
  unsigned int i;
  for ( i = 0;i < size; i++ ){
    printf("%d ",list[i]);
  }
  printf("\n\n");
}

unsigned char find_median(unsigned char* list,unsigned int size){
  unsigned int median_index;
  sort_array(list,size);        // First sort the array
  median_index = (size+1)/2  ;
  return list[median_index];
}

unsigned char find_mean(unsigned char* list,unsigned int size){
  unsigned int i;
  unsigned char mean;
  int sum = 0;
  sum = 0;
  for( i = 0; i < size ; i++){
    sum += list[i];
  }
  mean = (unsigned char)(sum/size);
  return mean;
}

unsigned char find_maximum(unsigned char* list,unsigned int size){
  unsigned int i;
  unsigned char max = 0; //assign the least possible value
  for(i = 0; i < size ;i++){
    if(list[i] > max){
      max = list[i];
    }
  }
  return max;
}

unsigned char find_minimum(unsigned char* list,unsigned int size){
  unsigned int i;
  unsigned char min = 255; //assign the largest possible value
  for(i = 0; i < size ;i++){
    if(list[i] < min){
      min = list[i];
    }
  }
  return min;
}

void sort_array(unsigned char* list,unsigned int size){
  unsigned int i,j;
  unsigned char temp;
  for (i = 0;i < size; i++){
    for (j = 0;j < size; j++){
      if (list[i] > list[j]){
        temp = list[i];
        list[i] = list[j];
        list[j] = temp;
      }
    }
  }

}